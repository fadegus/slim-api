<?php


class DataManager
{

    public function __construct() {}


    public function getUsers()
    {
        return json_decode(file_get_contents('../Manager/db.json'), true);
    }

    public function getUserById($id)
    {
        $users = $this->getUsers();
        foreach ($users as $user) {
            if ($user['id'] == $id) {
                return $user;
            }
        }
        return null;
    }

    public function createUser($username)
    {
        $users = $this->getUsers();

        $sha1 = sha1(rand().microtime());

        $data_user_json = array("id"=>$sha1, "username"=>$username);


        $users[] = $data_user_json;



        $this->putJson($users);

        return $users;



    }

    public function updateUser($data, $id)
    {
        $updateUser = [];
        $users = $this->getUsers();
        foreach ($users as $i => $user) {
            if ($user['id'] == $id) {
                $users[$i] = $updateUser = array_merge($user, array("username"=>$data));
            }
        }

        $this->putJson($users);

        return $updateUser;
    }

    public function deleteUser($id)
    {
        $users = $this->getUsers();

        foreach ($users as $i => $user) {
            if ($user['id'] == $id) {
                array_splice($users, $i, 1);
            }
        }

        $this->putJson($users);
    }



    public function putJson($users)
    {
        file_put_contents('../Manager/db.json', json_encode($users, JSON_PRETTY_PRINT));
    }






}