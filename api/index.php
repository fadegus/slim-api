<?php

require_once '../Manager/DataManager.php';
require_once '../Data/Data.php';
require '../vendor/autoload.php';

$app = new \Slim\App();

$app->get('/users', function($req, $resp, $args) use ($app){
    $db = new DataManager();
    $result = $db->getUsers();

    $response = array();
    $response['data'] = $result;

    return $resp->withStatus(200)->withJson($response);

});

$app->get('/user/{user_id}', function($req, $resp, $args) use ($app){
    $db = new DataManager();
    $result = $db->getUserById($args['user_id']);

    $response = array();
    $response['data'] = $result;

    return $resp->withStatus(200)->withJson($response);

});


$app->put('/user', function($req, $resp){

    $data = $req->getParsedBody();
    $username = $data['username'];

    $db = new DataManager();
    $res = $db->createUser($username);

    $resp->getBody()->write("$username ");
    return $resp;

});

$app->put('/user/{id}', function($req, $resp, $args) use ($app){


    $data = $req->getParsedBody();
    $username = $data['username'];

    $response = array();
    $db = new DataManager();
    $res = $db->updateUser($username, $args['id']);

    $response['data'] = array();

    if ($res) {
        $response["is_success"] = true;
        $response["message"] = "user updated";
        $response['data'] = $res;
        return $resp->withStatus(201)->withJson($response);
    } else{
        $response["is_success"] = false;
        $response["message"] = "BAD ERROR";
        return $resp->withStatus(200)->withJson($response);
    }
});




$app->delete('/user/{id}', function($req, $resp, $args) use ($app){

    $response = array();

    $db = new DataManager();


    if($db->getUserById($args['id'])){

        $res = $db->deleteUser($args['id']);

        $response["is_success"] = true;
        return $resp->withStatus(200)->withJson($response);


    }else{
        $response["is_success"] = false;
        $response['message'] = "No data found";
        return $resp->withStatus(404)->withJson($response);
    }




});



$app->run();